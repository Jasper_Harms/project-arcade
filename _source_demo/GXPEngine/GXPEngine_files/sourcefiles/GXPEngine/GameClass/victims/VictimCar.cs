﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXPEngine;

public class VictimCar : Sprite
{
    const double SCORE_PER_HIT = 100;
    const float SPEED_MULTIPLiER = 4f;
    private float _speed;
    private float _startingRotation;
    private float _startingX;
    private float _startingY;
    private bool _gotCoords;
    private float _strapsLength;

    private Level _level;
    private Projectile _poop;

    public bool poopedOn;
    public string facing;

    public VictimCar(Level pLevel, string pFacing) : base (RandomAssets.GetRandomCar())
    {
        SetOrigin(width / 2, height / 2);
        facing = pFacing;
        if (facing == "up") rotation = 180;
        if (facing == "down") rotation = 0;
        _startingRotation = rotation;
        _gotCoords = false;
        poopedOn = false;
        _strapsLength = 25;
        _level = pLevel;
    }
    private void Update()
    {
        GetStartingCoordinates();
        //NormalRotation();
        GotPoopedOn();
        GotOutOfTheWindow();
    }


    private void GotPoopedOn()
    {
        _speed = Difficulty.GetScrollSpeed() * SPEED_MULTIPLiER;
        if (facing == "up") _speed *= (-1);
        y += _speed;

        if (_level.poopList.Count > 0)
        {
            _poop = _level.poopList[0] as Projectile;

            if (HitTest(_poop))
            {
                if (_poop.GetScale() <= 0.4f)
                {
                    SFX.getHit.Play();
                    _poop.Destroy();
                    SetColor(0.1f, 0.1f, 0.1f);
                    _level.player.score += SCORE_PER_HIT;
                    poopedOn = true;
                }
            }
        }
    }

    private void GotOutOfTheWindow()
    {
        if (y >= game.height + game.height || y <= 0 - game.height * 2)
        {
            Destroy();
            _level.carsList.Remove(this);
        }
    }

    public float CorrectLane(float pX, string pFacing)
    {
        
        if (pFacing == "up")
        {
            //Right Lane
            pX = MyGame.OldX() / 2 + _strapsLength + width / 2;
        }
        if (pFacing == "down")
        {
            //Left Lane
            pX = MyGame.OldX() / 2 - _strapsLength - width / 2;
        }
        return pX;
    }


    public void SetPositionCar(VictimCar pCar, float pX, float pY)
    {
        pCar.SetXY(pX, pY);
        if (_level.carsList.Count > 0)
        {
            foreach (VictimCar otherCar in _level.carsList)
            {
                if (pCar.HitTest(otherCar))
                {
                    SetPositionCar(pCar, pCar.CorrectLane(pX, pCar.facing), pY);
                }
            }
        }
    }

    private void GetStartingCoordinates()
    {
        if (!_gotCoords)
        {
            _startingX = x;
            _startingY = y;
            _gotCoords = true;
        }
    }


}
