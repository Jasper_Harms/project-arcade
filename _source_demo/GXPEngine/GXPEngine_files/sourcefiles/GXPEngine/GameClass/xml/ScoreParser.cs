﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;

public class Parser
{
    public Parser()
    {

    }

    public HighScore Parse(string filename)
    {
        XmlSerializer xs = new XmlSerializer(typeof(HighScore));

        TextReader tr = new StreamReader(filename);
        HighScore hs = xs.Deserialize(tr) as HighScore;
        tr.Close();

        return hs;
    }
}
